package br.jdorigao.application;

import br.jdorigao.board.Board;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {

    public static void main(String[] args) {
        Board board = new Board();
        printBoard(board.getBoard());
    }

    public static void printBoard(char[][] board) {
        System.out.printf(" %c | %c | %c  %n", board[0][0], board[0][1], board[0][2]);
        System.out.println("---+---+---");
        System.out.printf(" %c | %c | %c  %n", board[1][0], board[1][1], board[1][2]);
        System.out.println("---+---+---");
        System.out.printf(" %c | %c | %c  %n", board[2][0], board[2][1], board[2][2]);
    }


}